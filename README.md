# What is SapphBot?
I'm glad you asked, other me. SapphBot is a hyper-specialised telegram bot. 
It queries the derpibooru servers in order to get my favourites, and in order to post them on telegram. It can do that with any query, but it's currently hard-coded.
# Are there plans to support other queries?
Yeah, but I'd just fork this project. This is my bot, that does things for me.
