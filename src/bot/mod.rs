use std::collections::HashSet;
use std::error::Error;
use std::fs::File;
use std::sync::mpsc;
use std::thread;

mod communicator;
mod derpiquery;

use self::communicator::Communicator;
use self::derpiquery::Derpiquery;
pub struct Bot {
    derpiquery: Derpiquery,
    communicator: Communicator,
}

#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq, Hash)]
pub struct Chat {
    pub chat_name: String,
    pub filter: String,
    pub requester: i64,
}

impl Chat {
    fn new(name: &str, re: &str, requester: i64) -> Chat {
        Chat {
            chat_name: name.to_owned(),
            filter: re.to_owned(),
            requester,
        }
    }
}

const TOKEN_PATH: &str = "token.txt";
const DERPI_API_KEY: &str = "derpi_api_key.txt";
const CHATS_PATH: &str = "chats.json";
const IMAGES_PATH: &str = "images.txt";
const HASH_LENGTH: usize = 512;
fn read_to_string(path: &str) -> Result<String, ErrorString> {
    match std::fs::read_to_string(&path) {
        Ok(string) => Ok(string),
        Err(err) => {
            let err_string = format!("Error reading file {}; Reason: {}", path, err);
            Err(ErrorString(err_string))
        }
    }
}

#[derive(Debug)]
struct ErrorString(String);
impl Error for ErrorString {}

impl ErrorString {
    fn boxed_error(string: String) -> Box<ErrorString> {
        Box::new(ErrorString(string))
    }
}

impl std::fmt::Display for ErrorString {
    fn fmt(&self, formatter: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        let ErrorString(ref string) = &self;
        formatter.write_str(string)
    }
}

impl Bot {
    pub fn new() -> Result<Bot, Box<Error>> {
        // Required files; return Err if reading them/it fails
        let token = read_to_string(TOKEN_PATH)?;
        let key = read_to_string(DERPI_API_KEY)?;

        // Optional files; if they fail to read
        let chats: HashSet<Chat> = match read_to_string(CHATS_PATH) {
            Ok(chats_json) => serde_json::from_str(&chats_json)?,
            Err(err) => {
                w!("Failed to read saved chats", err);
                HashSet::new()
            }
        };
        let raw_images = match read_to_string(IMAGES_PATH) {
            Ok(string) => string,
            Err(ErrorString(err)) => {
                w!("Failed to read saved images", err);
                String::new()
            }
        };
        let images = raw_images
            .lines()
            .filter(|s| s.len() != HASH_LENGTH / 2)
            .map(String::from);
        let derpiquery = Derpiquery::new(images.collect(), key);
        let communicator = Communicator::new(token, chats);
        Ok(Bot {
            derpiquery,
            communicator,
        })
    }

    pub fn run(self) -> Result<(), Box<Error>> {
        let Bot {
            mut derpiquery,
            mut communicator,
        } = self;
        let (sender, receiver) = mpsc::channel();
        let t1 = thread::spawn(move || {
            derpiquery.run(sender);
        });

        let t2 = thread::spawn(move || {
            communicator.run(receiver);
        });

        t1.join().unwrap();
        t2.join().unwrap();
        Ok(())
    }
}
