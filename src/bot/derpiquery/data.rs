#[derive(Debug, Serialize, Deserialize)]
pub struct Response {
    pub search: Vec<ImageResponse>,
}

#[derive(Debug, Hash, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct ImageResponse {
    pub id: i64,
    pub mime_type: String,
    pub image: String,
    pub representations: Representations,
    pub tags: String,
    pub source_url: String,
    pub sha512_hash: String,
}

#[derive(Debug, Hash, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct Representations {
    pub small: String,
    pub medium: String,
    pub large: String,
}
