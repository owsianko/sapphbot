use bot::*;
use std::io::Write;
use std::sync::mpsc::Sender;
use std::time::Duration;

pub mod data;
use self::data::ImageResponse;

use super::ErrorString;
pub struct Derpiquery {
    image_hashes: HashSet<String>,
    key: String,
}

impl Derpiquery {
    pub fn new(image_hashes: HashSet<String>, key: String) -> Derpiquery {
        Derpiquery { image_hashes, key }
    }
    pub fn run(&mut self, sender: Sender<Vec<ImageResponse>>) {
        // Set up exponential waiting
        let base_duration = Duration::from_secs(1);
        let max_duration = Duration::from_secs(180);
        let mut current_duration = base_duration;

        'updates: loop {
            // Sleep and increase sleep duration
            std::thread::sleep(current_duration);
            let next_duration = current_duration * 3 / 2;
            current_duration = if next_duration > max_duration {
                i!("Max Sleep Duration Reached");
                max_duration
            } else {
                next_duration
            };

            // Fetch images and update clients
            match self.fetch_images() {
                Ok(images) => {
                    if !images.is_empty() {
                        let (new_images, new_hashes) = self.compute_new_images(images);

                        if new_images.is_empty() {
                            i!("No new images")
                        } else {
                            current_duration = base_duration;
                            self.image_hashes = new_hashes;
                            self.save_image_hashes();
                            match sender.send(new_images) {
                                Err(err) => e!(err),
                                _ => (),
                            }
                        }
                    }
                }
                Err(err) => e!(err),
            };

            i!(format!("Timeout duration: {:?}", current_duration));
        }
    }

    /// Computes the new images, and returns the new total set
    fn compute_new_images(
        &self,
        images: HashSet<ImageResponse>,
    ) -> (Vec<data::ImageResponse>, HashSet<String>) {
        let mut new_images = HashSet::new();
        let mut new_image_hashes = HashSet::new();
        for image in images {
            if !self.image_hashes.contains(&image.sha512_hash) {
                new_image_hashes.insert(image.sha512_hash.clone());
                new_images.insert(image);
            }
        }

        let updated_images = self
            .image_hashes
            .union(&new_image_hashes)
            .cloned()
            .collect();
        i!("Fetched new images, new: ", new_images.len());
        (new_images.into_iter().collect(), updated_images)
    }

    fn save_image_hashes(&self) {
        match File::create(super::IMAGES_PATH) {
            Ok(mut file) => {
                for hash in &self.image_hashes {
                    if let Err(err) = writeln!(file, "{}", hash) {
                        e!("Error while writing file", err);
                    }
                }
            }
            Err(err) => e!("Error writing images to disk", err),
        }
    }

    /// Queries the derpibooru API for images
    fn fetch_images(&self) -> Result<HashSet<ImageResponse>, Box<Error>> {
        i!("Fetching new images");
        let url = format!(
            "https://derpibooru.org/search.json?q=my:faves&key={}",
            self.key
        );

        let mut resp = reqwest::get(&url)?;
        if resp.status().is_success() {
            if let Ok(body) = resp.text() {
                let v: data::Response = serde_json::from_str(&body).unwrap();
                i!(
                    "Successful response from server",
                    "Number of images gotten: ",
                    v.search.len()
                );
                Ok(v.search.iter().cloned().collect())
            } else {
                Err(ErrorString::boxed_error(format!(
                    "Error getting body {:?}",
                    resp
                )))
            }
        } else {
            Err(ErrorString::boxed_error(format!(
                "Unsuccesful response from server, {:?}",
                resp
            )))
        }
    }
}
